package org.store.product.common;

import java.util.function.Function;

import org.store.product.dto.ProductDto;
import org.store.product.model.Product;

public class AppUtils {
	public static Function<ProductDto, Product> productDtoToModel = new Function<ProductDto, Product>() {
		@Override
		public Product apply(ProductDto productDto) {
			Product product = new Product();
			product.setId(productDto.getId());
			product.setName(productDto.getName());
			product.setDescription(productDto.getDescription());
			product.setPrice(productDto.getPrice());
			product.setQuantity(productDto.getQuantity());
			product.setExpiryDate(productDto.getExpiryDate());
			product.setProductionDate(productDto.getProductionDate());
			return product;
		}
	};

	public static Function<Product, ProductDto> productModelToDto = new Function<Product, ProductDto>() {
		@Override
		public ProductDto apply(Product product) {
			ProductDto productDto = new ProductDto();
			productDto.setId(product.getId());
			productDto.setName(product.getName());
			productDto.setDescription(product.getDescription());
			productDto.setPrice(product.getPrice());
			productDto.setQuantity(product.getQuantity()); 
			productDto.setProductionDate(product.getProductionDate());
			productDto.setExpiryDate(product.getExpiryDate());
			return productDto;
		}
	};

}
