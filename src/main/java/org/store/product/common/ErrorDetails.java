package org.store.product.common;

import java.util.Date;

public class ErrorDetails {
	private Date timestamp;
	private String message;
	private String requestPath;

	public ErrorDetails(Date timestamp, String message, String requestPath) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.requestPath = requestPath;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public String getRequestPath() {
		return requestPath;
	}

}
