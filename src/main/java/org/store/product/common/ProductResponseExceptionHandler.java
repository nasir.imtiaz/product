package org.store.product.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ProductResponseExceptionHandler extends ResponseEntityExceptionHandler {
	Logger logger = LoggerFactory.getLogger(ProductResponseExceptionHandler.class);
	
	@Autowired
    private MessageSource messageSource;
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
	    List<ErrorDetails> errorDetails = new ArrayList<>();
	    
	    for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
	    	errorDetails.add(new ErrorDetails(new Date(), getMessage(violation.getMessage()), request.getDescription(false)));
	    }
	    
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<Object> handleConstraintViolation(DataIntegrityViolationException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), getMessage("error.duplicate.record"), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
	    ErrorDetails errorDetails = new ErrorDetails(new Date(), getMessage("error.unable.to.process.request"), request.getDescription(false));
	    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({ TransactionSystemException.class })
	public ResponseEntity<Object> handleConstraintViolation(Exception ex, WebRequest request) {
		List<ErrorDetails> errorDetails = new ArrayList<>();
	    Throwable cause = ((TransactionSystemException) ex).getRootCause();
	    
	    if (cause instanceof ConstraintViolationException) {
	        Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) cause).getConstraintViolations();
	        
		    for (ConstraintViolation<?> violation : constraintViolations) {
		    	errorDetails.add(new ErrorDetails(new Date(), getMessage(violation.getMessage()), request.getDescription(false)));
		    }
	    } else {
	    	errorDetails.add(new ErrorDetails(new Date(), getMessage("error.unable.to.process.request"), request.getDescription(false)));
	    }
	    
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ProductNotFoundException.class)
	public final ResponseEntity<Object> handleAllExceptions(ProductNotFoundException ex, WebRequest request) {
	    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
	    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
	
	private String getMessage(String key) {
		return messageSource.getMessage(key, new Object[]{}, key, Locale.getDefault());
	}
}
