package org.store.product.service;

import java.util.List;

import org.store.product.common.ProductNotFoundException;
import org.store.product.dto.ProductDto;

public interface ProductService {
	public ProductDto retrieveById(Integer id) throws ProductNotFoundException;
	public List<ProductDto> retrieveAll();
	public void delete(Integer id) throws ProductNotFoundException;
	public Integer create(ProductDto productDto);
	public Integer update(Integer id, ProductDto productDto) throws ProductNotFoundException;
}
