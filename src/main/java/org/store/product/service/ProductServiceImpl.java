package org.store.product.service;

import static org.store.product.common.AppUtils.productDtoToModel;
import static org.store.product.common.AppUtils.productModelToDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.store.product.common.ProductNotFoundException;
import org.store.product.dto.ProductDto;
import org.store.product.model.Product;
import org.store.product.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Integer create(ProductDto productDto) {
		productDto.setId(null);
		Product product = productRepository.save(productDtoToModel.apply(productDto));
		return product.getId();
	}
	
	@Override
	public Integer update(Integer id, ProductDto productDto) throws ProductNotFoundException {
		retrieveById(id);
		
		productDto.setId(id);
		
		Product product = productRepository.save(productDtoToModel.apply(productDto));
		return product.getId();
	}
	
	@Override
	public void delete(Integer id) throws ProductNotFoundException {
		retrieveById(id);
		
		productRepository.deleteById(id);
	}

	@Override
	public List<ProductDto> retrieveAll() {
		List<ProductDto> listProductDto = new ArrayList<>();
		List<Product> products = productRepository.findAll();
		
		if ( !products.isEmpty() ) {
			listProductDto = products.stream().map(productModelToDto).collect(Collectors.toList());
		}
		
		return listProductDto;
	}

	@Override
	public ProductDto retrieveById(Integer id) throws ProductNotFoundException {
		Optional<Product> product = productRepository.findById(id);
		
		if (product.isPresent()) {
			return productModelToDto.apply(product.get());
		} else {
			throw new ProductNotFoundException();
		}
		
	}
	
}
