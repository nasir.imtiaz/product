package org.store.product.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column( unique = true , nullable = false )
	@NotBlank(message="error.name.not.null.empty")
	private String name;
	
	@Size(max=250, message="error.description.long.length")
	private String description;
	
	@DecimalMin(value="0.0", inclusive=false, message="error.price.must.more.than.zero")
	@NotNull(message="error.price.not.null")
	private Double price;
	
	@Positive(message="error.quantity.must.more.than.zero")
	@NotNull(message="error.quantity.not.null")
	private Integer quantity;
	
	@NotNull(message="error.production.date.not.null")
	@PastOrPresent(message="error.production.date.not.future")
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate productionDate;
	
	@NotNull(message="error.expiry.date.not.null")
	@FutureOrPresent(message="error.expiry.date.not.future")
	private LocalDate expiryDate;

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public LocalDate getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(LocalDate productionDate) {
		this.productionDate = productionDate;
	}

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}
