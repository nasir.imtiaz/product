package org.store.product.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.store.product.common.ProductNotFoundException;
import org.store.product.dto.ProductDto;
import org.store.product.service.ProductService;

@RestController
@RequestMapping("/api")
public class ProductController {
	Logger logger = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/product")
	public List<ProductDto> getAll() {
		logger.info("Retrieving all products...");
		return productService.retrieveAll();
	}
	
	@GetMapping("/product/{id}")
	public ProductDto getProduct(@PathVariable Integer id) throws ProductNotFoundException {
		logger.info("Retrieving products by id {}", id);
		return productService.retrieveById(id);
	}

	@PostMapping("/product")
	@ResponseStatus( HttpStatus.CREATED )
	public Integer create(@RequestBody ProductDto product) {
		logger.info("Creating product={}", product);
		return productService.create(product);
	}

	@PutMapping("/product/{id}")
	public void update(@PathVariable Integer id, @RequestBody ProductDto product) throws ProductNotFoundException {
		logger.info("Updating product for id {}, product {}", id, product);
		productService.update(id, product);
	}

	@DeleteMapping("/product/{id}")
	public void delete(@PathVariable Integer id) throws ProductNotFoundException {
		logger.info("Removing product against Id={}", id);
		productService.delete(id);
	}
}