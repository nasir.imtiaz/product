package org.store.product;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ProductApplication.class)
@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
@SpringBootTest
public class ProductControllerTest {
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext wac;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	public void verifyGetAllProducts() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/").accept(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$", hasSize(3))).andDo(print());
	}

	@Test
	public void verifyGetProductById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/3").accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(jsonPath("$.id").exists()).andExpect(jsonPath("$.name").exists())
				.andExpect(jsonPath("$.price").exists()).andExpect(jsonPath("$.quantity").exists())
				.andExpect(jsonPath("$.productionDate").exists()).andExpect(jsonPath("$.expiryDate").exists());
	}
	
	@Test
	public void verifyGetProductByIdNotFound() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/99").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isNotFound()).andDo(print());
	}
	
	@Test
	public void verifyCreateProduct() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated()).andDo(print());
	}

	@Test
	public void verifyCreateProductWithId() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"id\":1, \"name\":\"Theta\", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void verifyCreateNullProduct() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void verifyCreateProductWithLongDescription() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"name\":\"apples\", \"description\":\"ABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABC\", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void verifyCreateProductWithNullName() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("name must not be null or empty"));
	}
	
	@Test
	public void verifyCreateProductWithEmptyName() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\" \", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("name must not be null or empty"));
	}
	
	@Test
	public void verifyCreateProductWithNullQuantity() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("quantity must not be null"));
	}
	
	@Test
	public void verifyCreateProductWithNullPrice() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("price must not be null"));
	}
	
	@Test
	public void verifyCreateProductWithNullProductionDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":9.1, \"expiryDate\":\"2018-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("production date must not be null"));
	}
	
	@Test
	public void verifyCreateProductWithNullExpiryDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("expiry date must not be null"));
	}
	
	@Test
	public void verifyCreateProductWithPriceEqualToZero() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":0.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("price must be more than zero"));
	}
	
	@Test
	public void verifyCreateProductWithPriceLessThanZero() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":-10.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("price must be more than zero"));
	}
	
	@Test
	public void verifyCreateProductWithQuantityLessThanZero() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\":-5, \"price\":11.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("quantity must be more than zero"));
	}
	
	@Test
	public void verifyCreateProductWithQuantityEqualToZero() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\":0, \"price\":11.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("quantity must be more than zero"));
	}
	
	@Test
	public void verifyCreateProductWithFutureProductionDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\":20, \"price\":11.0, \"productionDate\":\"2019-12-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("production date must not be in future"));
	}
	
	@Test
	public void verifyCreateProductWithPastExpiryDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\":20, \"price\":11.0, \"productionDate\":\"2017-12-02\", \"expiryDate\":\"2017-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("expiry date must not be in past"));
	}
	
	@Test
	public void verifyCreateProductWithDuplicateName() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"Alpha\", \"quantity\":20, \"price\":11.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2020-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message").value("duplicate record found"));
	}
	
	@Test
	public void verifyUpdateProduct() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void verifyUpdateProductWithLongDescription() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"name\":\"apples\", \"description\":\"ABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABC\", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void verifyUpdateWithNullProduct() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void verifyUpdateWithNullProductId() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/")
				.content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isMethodNotAllowed());
	}
	
	@Test
	public void verifyUpdateProductWithNullName() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isBadRequest())
		.andExpect(jsonPath("[0].message").value("name must not be null or empty"));
	}

	@Test
	public void verifyUpdateProductWithEmptyName() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\" \", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("name must not be null or empty"));
	}
	
	@Test
	public void verifyUpdateProductWithNullQuantity() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"price\":9.1, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("quantity must not be null"));
	}
	
	@Test
	public void verifyUpdateProductWithNullPrice() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("price must not be null"));
	}
	
	@Test
	public void verifyUpdateProductWithNullProductionDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":9.1, \"expiryDate\":\"2018-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("production date must not be null"));
	}
	
	@Test
	public void verifyUpdateProductWithNullExpiryDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":9.1, \"productionDate\":\"2018-10-02\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("expiry date must not be null"));
	}
	
	@Test
	public void verifyUpdateProductWithPriceEqualToZero() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":0.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("price must be more than zero"));
	}
	
	@Test
	public void verifyUpdateProductWithPriceLessThanZero() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\": 25, \"price\":-10.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("price must be more than zero"));
	}
	
	@Test
	public void verifyUpdateProductWithQuantityLessThanZero() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\":-5, \"price\":11.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("quantity must be more than zero"));
	}
	
	@Test
	public void verifyUpdateProductWithQuantityEqualToZero() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\":0, \"price\":11.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("quantity must be more than zero"));
	}
	
	@Test
	public void verifyUpdateProductWithFutureProductionDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\":20, \"price\":11.0, \"productionDate\":\"2019-12-02\", \"expiryDate\":\"2019-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("production date must not be in future"));
	}
	
	@Test
	public void verifyUpdateProductWithPastExpiryDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"apples\", \"quantity\":20, \"price\":11.0, \"productionDate\":\"2017-12-02\", \"expiryDate\":\"2017-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("[0].message").value("expiry date must not be in past"));
	}
	
	@Test
	public void verifyUpdateProductWithDuplicateName() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content("{ \"name\":\"Alpha\", \"quantity\":20, \"price\":11.0, \"productionDate\":\"2018-10-02\", \"expiryDate\":\"2020-11-05\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message").value("duplicate record found"));
	}
	
	@Test
	public void verifyDeleteProductWithId() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/api/product/1")
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk());
	}
	
	@Test
	public void verifyDeleteProductWithoutId() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/api/product/")
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isMethodNotAllowed());
	}
}
