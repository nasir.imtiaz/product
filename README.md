# Product Management
Project to perform CRUD operations for a Product

This application provides a user interface to perform CRUD operations on Product. The front end communicates with REST services to carry on the required operations.

### Features
- A responsive interface to create, update, delete and list products 
- The interface is written using jQuery, Javascript alongwith Bootstrap
- The backend is a Spring Boot based application with support for Spring MVC, Spring Data
- Proper validations are performed at each layer
- Comprehensive Unit Tests are also provided

### How to run:
  - clone this project into your local directory: 
  `git clone https://gitlab.com/nasir.imtiaz/product.git`
  - Build the project: 
  `mvn clean install package`
  - Deploy the generated web archive i.e. product.jar of the project into Tomcat:
  - Access the main page `http://localhost:8080/product/index.html`

### Webservices:
  - `/api/product` - GET - retrieves all products from the datastore  
  - `/api/product/{productId}` - GET - retrieve a product by id
  - `/api/product` - POST - create a product
  - `/api/product/{productId}` - PUT - update a product by Id
  - `/api/product/{productId}` - DELETE - delete a product by id